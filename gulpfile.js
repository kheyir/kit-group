var gulp = require('gulp')
	uglify = require('gulp-uglify'),
	minifyCSS = require('gulp-minify-css'),
	concat = require('gulp-concat'),
	del = require('del'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass');
 
 
gulp.task('scripts',function(){
	return gulp.src([
			'src/js/jquery-3.4.1.min.js',
			'src/js/TweenMax.min.js',
			'src/js/owl.carousel.js',
			'src/js/owl.autoplay.js',
			'src/js/owl.navigation.js',
			'src/js/owl.animate.js',
			'src/js/owl.support.js',
			"src/js/select2.full.min.js",
			'src/js/lightbox.js',
			'src/js/main.js'
		])
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('src/js'))
});

gulp.task("minifyScripts", function() {
	return gulp.src("src/js/scripts.js")
		.pipe(uglify())
		.pipe(rename('main.min.js'))
		.pipe(gulp.dest('src/js'));
});

gulp.task("sass", function() {
	return gulp.src("src/css/style.sass")
	.pipe(sass().on('error', sass.logError))
    .pipe(rename('style.css'))
	.pipe(gulp.dest('src/css'))
});

gulp.task("sass-resp", function() {
	return gulp.src("src/css/responsive.sass")
	.pipe(sass().on('error', sass.logError))
    .pipe(rename('responsive.css'))
	.pipe(gulp.dest('src/css'))
});

gulp.task("styles", function() {
	return gulp.src([
		"src/css/normalize.css",
		"src/css/reset.css",
		"src/css/owl.carousel.min.css",
		"src/css/owl.theme.default.min.css",
		"src/css/core.css",
		"src/css/animate.css",
		"src/css/lightbox.css",
		"src/css/bootstrap-grid.css",
		// "src/css/daterangepicker.css",
		'src/css/select2.min.css',
		// 'src/fonts/sourcesanspro/sourcesanspro.css',
		"src/css/style.css",
		"src/css/responsive.css"
	])
	.pipe(minifyCSS())
	.pipe(concat('site.css'))
	.pipe(gulp.dest('src/css'))
});

gulp.task("minifyCss", function() {
  return gulp.src("src/css/site.css")
    .pipe(rename('site.css'))
    .pipe(gulp.dest('src/css'));
});

gulp.task('clean', function() {
  return del(['src/css/site.css']);
});

gulp.task('watch',function(){
	gulp.watch('src/js/main.js',['scripts', 'minifyScripts']);
    gulp.watch('src/css/*.sass',['sass', 'sass-resp', 'styles', 'minifyCss']);
});

gulp.task('default', ['scripts', 'minifyScripts', 'sass', 'styles', 'minifyCss', 'clean']);